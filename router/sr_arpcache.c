#include <netinet/in.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <pthread.h>
#include <sched.h>
#include <string.h>
#include "sr_arpcache.h"
#include "sr_router.h"
#include "sr_if.h"
#include "sr_rt.h"
#include "sr_protocol.h"
#include "sr_utils.h"



uint8_t* makeArp(struct sr_instance* sr){
  uint8_t* packet = malloc(sizeof(struct sr_arp_hdr) + sizeof(struct sr_ethernet_hdr));
  struct sr_ethernet_hdr* newEther = (struct sr_ethernet_hdr*) packet;
  struct sr_arp_hdr* newArp = (struct sr_arp_hdr*) (packet + 14);
  newArp->ar_hrd = 256;
  newArp->ar_pro = 8;
  newArp->ar_hln = 6;
  newArp->ar_pln = 4;
  newArp->ar_op = 256;
  int i;
  for ( i=0; i<6; i++){
    newArp->ar_tha[i] = 255;
    newEther->ether_dhost[i] = 255;
  }
  newEther->ether_type = ntohs(0x0806);
  
  return packet;
  
}

int getlength(uint32_t mask){
  int count =0;
  int index=0;
  int breaker=1;
  int bit;
  while (index<32 && breaker==1){
    bit = (mask >> (31 - index)) & 1;
    if (bit){
      count++;
    }
    else{
      breaker=0;
    }
    index++;
  }
  return count;
}

int match(uint32_t a, uint32_t b, int len){
  int index;
  int bita;
  int bitb;
  for (index = 0; index<len; index++){
    bita = (a >> (31-index)) & 1;
    bitb = (b >> (31-index)) & 1;
    if (bita != bitb){
      return 0;
    }
  }
  return 1;
}

struct sr_rt* getPath(struct sr_instance* sr, uint32_t destIP){
  struct sr_rt* currentRT = sr->routing_table;
  struct sr_rt* finalRT = NULL;
  int lcm = -1;
  int masklen = 0;
  while(currentRT){
    masklen = getlength(htonl(currentRT->mask.s_addr)); 
    if (match(htonl((currentRT->dest.s_addr)), htonl(destIP), masklen) == 1 && masklen>lcm){
      finalRT = currentRT;
      lcm = masklen;
    }
    currentRT=currentRT->next;
  }
  return finalRT;
}

void handle_arpreq(struct sr_instance* sr, struct sr_arpreq* current){
  if (current->times_sent < 5){
    struct sr_if* currentIF = sr->if_list;
    uint8_t* packet = makeArp(sr);
    struct sr_ethernet_hdr* ether = (struct sr_ethernet_hdr*)packet;
    struct sr_arp_hdr* arp = (struct sr_arp_hdr*)(packet+14);
    arp->ar_tip = current->ip;
    struct sr_rt* testRT = getPath(sr, arp->ar_tip);
    if (testRT){
      currentIF = sr_get_interface(sr, testRT->interface);
      memcpy(ether->ether_shost, currentIF->addr, 6);
      memcpy(arp->ar_sha, currentIF->addr, 6);
      arp->ar_sip = currentIF->ip;
      arp->ar_tip = current->ip;

      sr_send_packet(sr, packet, 42, currentIF->name);

      current->sent = time(0);
      current->times_sent++;
    }
    else{
      struct sr_packet* oldpacket = current->packets;
      while (oldpacket){
	print_addr_ip_int(current->ip);
	printf("Sending ICMP 5\n");
	struct sr_ethernet_hdr* curEth = (struct sr_ethernet_hdr*)oldpacket->buf;
	struct sr_ip_hdr* curip = (struct sr_ip_hdr*)(oldpacket->buf+14);
        
	uint8_t* newpacket = makeICMP(sr, 3, 0);
	struct sr_ethernet_hdr* newEther = (struct sr_ethernet_hdr*)(newpacket);
	struct sr_ip_hdr* newIP = (struct sr_ip_hdr*)(newpacket+14);
	struct sr_icmp_t3_hdr* newICMP = (struct sr_icmp_t3_hdr*)(newpacket+34);
	
	  
        newIP->ip_ttl = 64;
	  
        uint8_t macholder[6];
        memcpy(macholder, curEth->ether_shost, 6);
        memcpy(newEther->ether_shost, curEth->ether_dhost, 6);
        memcpy(newEther->ether_dhost, macholder, 6);

	struct sr_if* curIF = sr_get_interface(sr, oldpacket->iface);
        
        newIP->ip_src = curIF->ip;
	newIP->ip_dst = curip->ip_src;
        newIP->ip_ttl = 64;
        newIP->ip_hl = curip->ip_hl;
        newIP->ip_v = curip->ip_v;
        newIP->ip_tos = curip->ip_tos;		        
        newIP->ip_len = ntohs(0x0038);		        
        newIP->ip_id = ntohs(0x0000);		        
        newIP->ip_off = 0;
        newIP->ip_p = 1;
        newIP->ip_sum = 0;
        newIP->ip_sum = cksum(newIP, 20);

	newICMP->icmp_type = 3;
        newICMP->icmp_code = 0;
	newICMP->unused = 0;
	newICMP->next_mtu = 0;
	memcpy(newICMP->data, curip, 28);
        newICMP->icmp_sum = 0;
        newICMP->icmp_sum = cksum(newICMP, sizeof( struct sr_icmp_t3_hdr));
        sr_send_packet(sr, newpacket, sizeof(struct sr_ip_hdr) + sizeof(struct sr_ethernet_hdr) + sizeof(struct sr_icmp_t3_hdr), oldpacket->iface);

	
	oldpacket = NULL;
	sr_arpreq_destroy(&(sr->cache), current);
      
      }
    }
   
      
  }
    else{
      /*Send ICMP's*/
      printf("ICMP NOPE\n\n");
      sr_arpreq_destroy(&(sr->cache), current);
    }


}


void empty_req(struct sr_instance* sr, struct sr_arpreq* current){
  struct sr_packet * currentPacket = current->packets;
  struct sr_ip_hdr* ip;
  struct sr_arpentry* entry = sr_arpcache_lookup(&(sr->cache), current->ip);
  struct sr_ethernet_hdr* ether;
  struct sr_if* currentIF = sr->if_list;
  int index =1;
  while (currentPacket != NULL){
    ether = (struct sr_ethernet_hdr*)currentPacket->buf;
    ip = (struct sr_ip_hdr*)(currentPacket->buf +14);

    if (entry){
      memcpy(ether->ether_dhost, entry->mac, 6);
    }
    
    struct sr_rt* rt = getPath(sr, current->ip);
    currentIF = sr_get_interface(sr, rt->interface);
    memcpy(ether->ether_shost, currentIF->addr, 6);
    ip->ip_sum = 0;
    ip->ip_sum = cksum(ip, 20);
        
    print_hdrs(currentPacket->buf, currentPacket->len);
    sr_send_packet(sr, currentPacket->buf, currentPacket->len,currentPacket->iface); 
    index++;
    currentPacket = currentPacket->next;
  }
  
}


/* 
  This function gets called every second. For each request sent out, we keep
  checking whether we should resend an request or destroy the arp request.
  See the comments in the header file for an idea of what it should look like.
*/
void sr_arpcache_sweepreqs(struct sr_instance *sr) { 
  printf("SweepReqs!\n");
  struct sr_arpreq* current = sr->cache.requests;
  struct sr_arpreq* next;

  while (current != NULL){
    printf("%d--", current->times_sent);
    next = current->next;
    handle_arpreq(sr, current);
    current = next;
  }
  
}

/* You should not need to touch the rest of this code. */

/* Checks if an IP->MAC mapping is in the cache. IP is in network byte order.
   You must free the returned structure if it is not NULL. */
struct sr_arpentry *sr_arpcache_lookup(struct sr_arpcache *cache, uint32_t ip) {
    pthread_mutex_lock(&(cache->lock));
    
    struct sr_arpentry *entry = NULL, *copy = NULL;
    
    int i;
    for (i = 0; i < SR_ARPCACHE_SZ; i++) {
        if ((cache->entries[i].valid) && (cache->entries[i].ip == ip)) {
            entry = &(cache->entries[i]);
        }
    }
    
    /* Must return a copy b/c another thread could jump in and modify
       table after we return. */
    if (entry) {
        copy = (struct sr_arpentry *) malloc(sizeof(struct sr_arpentry));
        memcpy(copy, entry, sizeof(struct sr_arpentry));
    }
        
    pthread_mutex_unlock(&(cache->lock));
    
    return copy;
}

/* Adds an ARP request to the ARP request queue. If the request is already on
   the queue, adds the packet to the linked list of packets for this sr_arpreq
   that corresponds to this ARP request. You should free the passed *packet.
   
   A pointer to the ARP request is returned; it should not be freed. The caller
   can remove the ARP request from the queue by calling sr_arpreq_destroy. */
struct sr_arpreq *sr_arpcache_queuereq(struct sr_arpcache *cache,
                                       uint32_t ip,
                                       uint8_t *packet,           /* borrowed */
                                       unsigned int packet_len,
                                       char *iface)
{
    pthread_mutex_lock(&(cache->lock));
    
    struct sr_arpreq *req;
    for (req = cache->requests; req != NULL; req = req->next) {
        if (req->ip == ip) {
            break;
        }
    }
    
    /* If the IP wasn't found, add it */
    if (!req) {
        req = (struct sr_arpreq *) calloc(1, sizeof(struct sr_arpreq));
        req->ip = ip;
        req->next = cache->requests;
        cache->requests = req;
    }
    
    /* Add the packet to the list of packets for this request */
    if (packet && packet_len && iface) {
        struct sr_packet *new_pkt = (struct sr_packet *)malloc(sizeof(struct sr_packet));
        
        new_pkt->buf = (uint8_t *)malloc(packet_len);
        memcpy(new_pkt->buf, packet, packet_len);
        new_pkt->len = packet_len;
		new_pkt->iface = (char *)malloc(sr_IFACE_NAMELEN);
        strncpy(new_pkt->iface, iface, sr_IFACE_NAMELEN);
        new_pkt->next = req->packets;
        req->packets = new_pkt;
    }
    
    pthread_mutex_unlock(&(cache->lock));
    
    return req;
}

/* This method performs two functions:
   1) Looks up this IP in the request queue. If it is found, returns a pointer
      to the sr_arpreq with this IP. Otherwise, returns NULL.
   2) Inserts this IP to MAC mapping in the cache, and marks it valid. */
struct sr_arpreq *sr_arpcache_insert(struct sr_arpcache *cache,
                                     unsigned char *mac,
                                     uint32_t ip)
{
    pthread_mutex_lock(&(cache->lock));
    
    struct sr_arpreq *req, *prev = NULL, *next = NULL; 
    for (req = cache->requests; req != NULL; req = req->next) {
        if (req->ip == ip) {            
            if (prev) {
                next = req->next;
                prev->next = next;
            } 
            else {
                next = req->next;
                cache->requests = next;
            }
            
            break;
        }
        prev = req;
    }
    
    int i;
    for (i = 0; i < SR_ARPCACHE_SZ; i++) {
        if (!(cache->entries[i].valid))
            break;
    }
    
    if (i != SR_ARPCACHE_SZ) {
        memcpy(cache->entries[i].mac, mac, 6);
        cache->entries[i].ip = ip;
        cache->entries[i].added = time(NULL);
        cache->entries[i].valid = 1;
    }
    
    pthread_mutex_unlock(&(cache->lock));
    
    return req;
}

/* Frees all memory associated with this arp request entry. If this arp request
   entry is on the arp request queue, it is removed from the queue. */
void sr_arpreq_destroy(struct sr_arpcache *cache, struct sr_arpreq *entry) {
    pthread_mutex_lock(&(cache->lock));
    
    if (entry) {
        struct sr_arpreq *req, *prev = NULL, *next = NULL; 
        for (req = cache->requests; req != NULL; req = req->next) {
            if (req == entry) {                
                if (prev) {
                    next = req->next;
                    prev->next = next;
                } 
                else {
                    next = req->next;
                    cache->requests = next;
                }
                
                break;
            }
            prev = req;
        }
        
        struct sr_packet *pkt, *nxt;
        
        for (pkt = entry->packets; pkt; pkt = nxt) {
            nxt = pkt->next;
            if (pkt->buf)
                free(pkt->buf);
            if (pkt->iface)
                free(pkt->iface);
            free(pkt);
        }
        
        free(entry);
    }
    
    pthread_mutex_unlock(&(cache->lock));
}

/* Prints out the ARP table. */
void sr_arpcache_dump(struct sr_arpcache *cache) {
    fprintf(stderr, "\nMAC            IP         ADDED                      VALID\n");
    fprintf(stderr, "-----------------------------------------------------------\n");
    
    int i;
    for (i = 0; i < SR_ARPCACHE_SZ; i++) {
        struct sr_arpentry *cur = &(cache->entries[i]);
        unsigned char *mac = cur->mac;
        fprintf(stderr, "%.1x%.1x%.1x%.1x%.1x%.1x   %.8x   %.24s   %d\n", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5], ntohl(cur->ip), ctime(&(cur->added)), cur->valid);
    }
    
    fprintf(stderr, "\n");
}

/* Initialize table + table lock. Returns 0 on success. */
int sr_arpcache_init(struct sr_arpcache *cache) {  
    /* Seed RNG to kick out a random entry if all entries full. */
    srand(time(NULL));
    
    /* Invalidate all entries */
    memset(cache->entries, 0, sizeof(cache->entries));
    cache->requests = NULL;
    
    /* Acquire mutex lock */
    pthread_mutexattr_init(&(cache->attr));
    pthread_mutexattr_settype(&(cache->attr), PTHREAD_MUTEX_RECURSIVE);
    int success = pthread_mutex_init(&(cache->lock), &(cache->attr));
    
    return success;
}

/* Destroys table + table lock. Returns 0 on success. */
int sr_arpcache_destroy(struct sr_arpcache *cache) {
    return pthread_mutex_destroy(&(cache->lock)) && pthread_mutexattr_destroy(&(cache->attr));
}

/* Thread which sweeps through the cache and invalidates entries that were added
   more than SR_ARPCACHE_TO seconds ago. */
void *sr_arpcache_timeout(void *sr_ptr) {
    struct sr_instance *sr = sr_ptr;
    struct sr_arpcache *cache = &(sr->cache);
    
    while (1) {
        sleep(1.0);
        
        pthread_mutex_lock(&(cache->lock));
    
        time_t curtime = time(NULL);
        
        int i;    
        for (i = 0; i < SR_ARPCACHE_SZ; i++) {
            if ((cache->entries[i].valid) && (difftime(curtime,cache->entries[i].added) > SR_ARPCACHE_TO)) {
                cache->entries[i].valid = 0;
            }
        }
        
        sr_arpcache_sweepreqs(sr);

        pthread_mutex_unlock(&(cache->lock));
    }
    
    return NULL;
}

