
#include <signal.h>
#include <assert.h>
#include "sr_nat.h"
#include <unistd.h>
#include <stdlib.h> 
#include <stdio.h>
#include <string.h>
#include "sr_if.h"

int sr_nat_init(struct sr_nat *nat) { /* Initializes the nat */

  assert(nat);

  /* Acquire mutex lock */
  pthread_mutexattr_init(&(nat->attr));
  pthread_mutexattr_settype(&(nat->attr), PTHREAD_MUTEX_RECURSIVE);
  int success = pthread_mutex_init(&(nat->lock), &(nat->attr));
  
  /* Initialize timeout thread */

  pthread_attr_init(&(nat->thread_attr));
  pthread_attr_setdetachstate(&(nat->thread_attr), PTHREAD_CREATE_JOINABLE);
  pthread_attr_setscope(&(nat->thread_attr), PTHREAD_SCOPE_SYSTEM);
  pthread_attr_setscope(&(nat->thread_attr), PTHREAD_SCOPE_SYSTEM);
  pthread_create(&(nat->thread), &(nat->thread_attr), sr_nat_timeout, nat);

  /* CAREFUL MODIFYING CODE ABOVE THIS LINE! */

  nat->mappings = NULL;
  /* Initialize any variables here */
  nat->nextFreePort = malloc(3000 * sizeof(uint8_t));
  memset(nat->nextFreePort, 0, 3000);
  nat->ip_ext = -1;
  printf("It worked\n");
  return success;
}



int sr_nat_destroy(struct sr_nat *nat) {  /* Destroys the nat (free memory) */

  pthread_mutex_lock(&(nat->lock));

  /* free nat memory here */

  pthread_kill(nat->thread, SIGKILL);
  return pthread_mutex_destroy(&(nat->lock)) &&
    pthread_mutexattr_destroy(&(nat->attr));

}

void *sr_nat_timeout(void *nat_ptr) {  /* Periodic Timout handling */
  struct sr_nat *nat = (struct sr_nat *)nat_ptr;
  while (1) {
    sleep(1.0);
    pthread_mutex_lock(&(nat->lock));
    
    time_t curtime = time(NULL);

    struct sr_nat_mapping* leader = nat->mappings;
    while (leader && leader->last_updated < curtime - 10){
      if (leader->next == NULL){
	leader = NULL;
      }
      else{
	leader = leader->next;
      }
    }
    nat->mappings = leader;
    struct sr_nat_mapping* current = nat->mappings;
    struct sr_nat_mapping* exit = malloc(sizeof(struct sr_nat_mapping));
    exit->next == NULL;
    if (leader){
      while (leader->next){
	leader = leader->next;
	if (leader->last_updated < curtime - 10){
	  current->next = leader->next;
	}
	current =  current->next;
	leader = current;
	if (leader == NULL){
	  leader = exit;
	}
      }
    }

    pthread_mutex_unlock(&(nat->lock) );
  }
  return NULL;
}

/* Get the mapping associated with given external port.
   You must free the returned structure if it is not NULL. */
struct sr_nat_mapping *sr_nat_lookup_external(struct sr_nat *nat,
    uint16_t aux_ext, sr_nat_mapping_type type ) {
  pthread_mutex_lock(&(nat->lock));
  
  /* handle lookup here, malloc and assign to copy */
  struct sr_nat_mapping *copy = nat->mappings;
  while (copy){
    if (copy->aux_ext == aux_ext){
      struct sr_nat_mapping *freshCopy = malloc(sizeof(struct sr_nat_mapping));
      memcpy(freshCopy, copy, sizeof(struct sr_nat_mapping));
      return freshCopy;
    }
    copy = copy->next;
  }
  pthread_mutex_unlock(&(nat->lock));
  return NULL;
}

/* Get the mapping associated with given internal (ip, port) pair.
   You must free the returned structure if it is not NULL. */
struct sr_nat_mapping *sr_nat_lookup_internal(struct sr_nat *nat,
  uint32_t ip_int, uint16_t aux_int, sr_nat_mapping_type type ) {

  pthread_mutex_lock(&(nat->lock));
  /* handle lookup here, malloc and assign to copy. */
  struct sr_nat_mapping *copy = nat->mappings;
  while (copy){
    if (copy->ip_int == ip_int && copy->aux_int == aux_int){
      struct sr_nat_mapping *freshCopy = malloc(sizeof(struct sr_nat_mapping));
      memcpy(freshCopy, copy, sizeof(struct sr_nat_mapping));
      return freshCopy;
    }
    copy = copy->next;
  }

  pthread_mutex_unlock(&(nat->lock));
  return NULL;
}

/* Insert a new mapping into the nat's mapping table.
   Actually returns a copy to the new mapping, for thread safety.
 */
struct sr_nat_mapping *sr_nat_insert_mapping(struct sr_nat *nat,
  uint32_t ip_int, uint16_t aux_int, sr_nat_mapping_type type ) {

  pthread_mutex_lock(&(nat->lock));
  
  /* handle insert here, create a mapping, and then return a copy of it */
  struct sr_nat_mapping *mapping = malloc(sizeof(struct sr_nat_mapping));
  mapping->type= type;
  mapping->ip_int = ip_int;
  mapping->aux_int = aux_int;
  mapping->ip_ext = nat->ip_ext;
  int index = 0;
  
  while (nat->nextFreePort[index] == 1){
    index++;
  }
  nat->nextFreePort[index] = 1;
  mapping->aux_ext = index + 2000;
  mapping->last_updated = time(NULL);
  mapping->next = NULL;
  struct sr_nat_mapping *copy = nat->mappings;
  if (copy){
    while (copy->next){
      copy = copy->next;
    }
    copy->next = mapping;
  }
  else{
    nat->mappings = mapping;
  }
  struct sr_nat_mapping* mappingCopy = malloc(sizeof(struct sr_nat_mapping));
  memcpy(mappingCopy, mapping, sizeof(struct sr_nat_mapping));
  pthread_mutex_unlock(&(nat->lock));
  return mappingCopy;
}

void print_nat_table(struct sr_nat *nat){
  struct sr_nat_mapping *map = nat->mappings;
  printf("\n----------------------------\n");
  while (map){
    if (map){
      printf("ip_int %d ip_ext %d aux_int %d aux_ext %d \n \n\n", map->ip_int, map->ip_ext, map->aux_int, map->aux_ext);
      map = map->next;
    }
  }
  printf("---------------------------\n");
}
