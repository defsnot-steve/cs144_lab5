struct sr_tcp_hdr
{
  uint16_t s_port;
  uint16_t d_port;
  uint32_t seq;
  uint32_t ack;
  uint8_t offest;
  uint8_t flags;
  uint16_t window;
  uint16_t sum;
  uint16_t urgent;
} __attribute__ ((packed)) ;
typedef struct sr_tcp_hdr sr_tcp_hdr_t;
