/********************************************************************
 * file:  sr_router.c
 * date:  Mon Feb 18 12:50:42 PST 2002
 * Contact: casado@stanford.edu
 *
 * Description:
 *
 * This file contains all the functions that interact directly
 * with the routing table, as well as the main entry method
 * for routing.
 *
 **********************************************************************/

#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "sr_if.h"
#include "sr_rt.h"
#include "sr_nat.h"
#include "sr_router.h"
#include "sr_protocol.h"
#include "sr_arpcache.h"
#include "sr_utils.h"
#include "tcp.h"


/*---------------------------------------------------------------------
 * Method: sr_init(void)
 * Scope:  Global
 *
 * Initialize the routing subsystem
 *
 *---------------------------------------------------------------------*/

void sr_init(struct sr_instance* sr)
{
    /* REQUIRES */
    assert(sr);

    /* Initialize cache and cache cleanup thread */
    sr_arpcache_init(&(sr->cache));

    pthread_attr_init(&(sr->attr));
    pthread_attr_setdetachstate(&(sr->attr), PTHREAD_CREATE_JOINABLE);
    pthread_attr_setscope(&(sr->attr), PTHREAD_SCOPE_SYSTEM);
    pthread_attr_setscope(&(sr->attr), PTHREAD_SCOPE_SYSTEM);
    pthread_t thread;

    pthread_create(&thread, &(sr->attr), sr_arpcache_timeout, sr);
    
    /* Add initialization code here! */
    /*sr_nat_init(&(sr->nat));*/
    
} /* -- sr_init -- */

void sendICMP(struct sr_instance* sr, uint8_t* packet, struct sr_if* inter, struct sr_rt* forwardRT, uint32_t ip_int){
  uint8_t* newBuffer = malloc(70);
  uint32_t outgoing = sr_get_interface(sr, "eth2")->ip;
  uint32_t incoming = sr_get_interface(sr, "eth1")->ip;
  struct sr_ethernet_hdr* ether = (struct sr_ethernet_hdr* )newBuffer;
  struct sr_ip_hdr* ip = (struct sr_ip_hdr* )(newBuffer + 14);
  struct sr_icmp_t3_hdr* icmp = (struct sr_icmp_t3_hdr*)(newBuffer + 34);

  struct sr_ethernet_hdr* old_ether = (struct sr_ethernet_hdr* )packet;
  struct sr_ip_hdr* old_ip = (struct sr_ip_hdr* )(packet + 14);
  struct sr_tcp_hdr* old_tcp = (struct sr_tcp_hdr* )(packet + 34);

  memcpy(ether->ether_shost, inter->addr, 6);
  ether->ether_type = old_ether->ether_type;
  ip->ip_ttl = 64;
  ip->ip_hl = old_ip->ip_hl;
  ip->ip_v = old_ip->ip_v;
  ip->ip_tos = old_ip->ip_tos;		        
  ip->ip_len = ntohs(0x0038);		        
  ip->ip_id = ntohs(0x0000);		        
  ip->ip_off = old_ip->ip_off;
  ip->ip_p = 1;
  
  
  ip->ip_dst = old_ip->ip_src;
  ip->ip_src = old_ip->ip_dst;
  ip->ip_sum = 0;
  ip->ip_sum = cksum(ip, 20);
  icmp->icmp_type = 3;
  icmp->icmp_code = 3;
  icmp->unused = 0;
  icmp->next_mtu = 0;
  memcpy(icmp->data, old_ip, 28);
  icmp->icmp_sum = 0;
  icmp->icmp_sum = cksum(icmp, sizeof(struct sr_icmp_t3_hdr));

  struct sr_arpentry* entry;
  if (forwardRT){
		  
    entry = sr_arpcache_lookup(&(sr->cache), ip_int);
  }
  else{
    entry = NULL;
  }
  if (entry){
    memcpy(ether->ether_dhost, entry->mac, 6);
	   
    sr_send_packet(sr, newBuffer, 70, inter->name);
  }
  else{
    struct sr_arpreq* req;
    req = sr_arpcache_queuereq(&(sr->cache), ip_int ,newBuffer, 70, "eth1");
    handle_arpreq(sr, req);
  }
  

}

void natICMP(struct sr_instance* sr, uint8_t* packet, struct sr_if* inter, unsigned int len) {
	struct sr_ip_hdr* header_ip = (struct sr_ip_hdr*)(packet+14);
	struct sr_ethernet_hdr* ethernet_hdr = (struct sr_ethernet_hdr*)(packet);
	uint32_t internalIP = sr_get_interface(sr, "eth1")->ip;
	uint32_t externalIP = sr_get_interface(sr, "eth2")->ip;
	struct sr_icmp_t3_hdr* icmp_header = (struct sr_icmp_t3_hdr*)(packet + 34);
	struct sr_nat_mapping* lookUp;
	/*Check if interface is internal NAT interface*/
	if (internalIP == inter->ip){
		printf("ICMP from internal to External");
		/*Look up mapping*/
	        lookUp = sr_nat_lookup_internal(sr->nat, header_ip->ip_src, icmp_header->unused, nat_mapping_icmp);
		/*If mapping does not exist, insert a mapping*/
		if (lookUp == NULL) {
			printf("Mapping does not exist");
			lookUp = sr_nat_insert_mapping(sr->nat, header_ip->ip_src, icmp_header->unused, nat_mapping_icmp);
		}
	}
	if (externalIP == inter->ip){
	  printf("ICMP from Extrernal to Internal");
		/*Look up mapping*/
	        lookUp = sr_nat_lookup_external(sr->nat, icmp_header->unused, nat_mapping_icmp);
		/*If mapping does not exist, insert a mapping*/
		if (lookUp == NULL) {
			printf("Mapping does not exist");
			lookUp = sr_nat_insert_mapping(sr->nat, header_ip->ip_src, icmp_header->unused, nat_mapping_icmp);
		}
	}
	struct sr_rt* testRT = getPath(sr, header_ip->ip_dst);
	/*Rewrite ICMP/IP header to prepare to send*/;
	if (internalIP == inter->ip){
	  icmp_header->unused = lookUp->aux_ext;
	}else{
	  icmp_header->unused = lookUp->aux_int;
	}
	header_ip->ip_ttl = header_ip->ip_ttl - 1;
	if (internalIP == inter->ip){
	  header_ip->ip_src = externalIP;
	}else{
	  header_ip->ip_dst = lookUp->ip_int;
	}
	header_ip->ip_sum = 0;
	header_ip->ip_sum = cksum(header_ip, 20);
	icmp_header->icmp_sum = 0;
	icmp_header->icmp_sum = cksum(icmp_header, len - 34);
	
	struct sr_if* sender;
	/*Set the new Source Mac Address*/
	if (internalIP == inter->ip){
	  sender = sr_get_interface(sr, testRT->interface);
	}
	else{
	  sender = sr_get_interface(sr, "eth1");
	}
	memcpy(ethernet_hdr->ether_shost, sender->addr, 6);

	/*Arp request for the next Destination*/
	struct sr_arpentry* entry;
	struct sr_rt* forwardRT;
	if (internalIP == inter->ip){
	  forwardRT = getPath(sr, header_ip->ip_dst);
	}
	else {
	  forwardRT = getPath(sr, lookUp->ip_int);
	}
	if (internalIP == inter->ip){
	  if (forwardRT){
		  
	    entry = sr_arpcache_lookup(&(sr->cache), forwardRT->dest.s_addr);
	  }
	  else{
	    entry = NULL;
	  }
	  if (entry){
		  
	    memcpy(ethernet_hdr->ether_dhost, entry->mac, 6);
		  
	    sr_send_packet(sr, packet, len, testRT->interface);
	  }
	  else{
	    struct sr_arpreq* req;
	    req = sr_arpcache_queuereq(&(sr->cache), header_ip->ip_dst,packet, len, testRT->interface);
	    handle_arpreq(sr, req);
	  }
	}
	else{
	  if (forwardRT){
		  
	    entry = sr_arpcache_lookup(&(sr->cache), lookUp->ip_int);
	  }
	  else{
	    entry = NULL;
	  }
	  if (entry){
	    memcpy(ethernet_hdr->ether_dhost, entry->mac, 6);
	   
	    sr_send_packet(sr, packet, len, sender->name);
	  }
	  else{
	    struct sr_arpreq* req;
	    req = sr_arpcache_queuereq(&(sr->cache), lookUp->ip_int ,packet, len, "eth1");
	    handle_arpreq(sr, req);
	  }

	}
	printf("Preparing to send packet to destination");
	print_nat_table(sr->nat);
	/*free(lookUp);*/
	/*Check if External*/
}

void natTCP (struct sr_instance* sr, uint8_t* packet, struct sr_if* inter, unsigned int len){
  struct sr_ip_hdr* header_ip = (struct sr_ip_hdr*)(packet+14);
  struct sr_ethernet_hdr* ethernet_hdr = (struct sr_ethernet_hdr*)(packet);
  uint32_t internalIP = sr_get_interface(sr, "eth1")->ip;
  uint32_t externalIP = sr_get_interface(sr, "eth2")->ip;
  struct sr_tcp_hdr* tcp_header = (struct sr_tcp_hdr*)(packet + sizeof(struct sr_ip_hdr) + sizeof(struct sr_ethernet_hdr));
  struct sr_nat_mapping* lookUp;
  struct sr_rt* testRT = getPath(sr, header_ip->ip_dst);
  
  if (header_ip->ip_dst == internalIP){
    struct sr_rt* returnTrip  = getPath(sr, header_ip->ip_dst);
    sendICMP(sr, packet,sr_get_interface(sr,"eth1"), returnTrip, header_ip->ip_src);
	  

  }
  else{
    if (internalIP == inter->ip){
      printf("TCP from internal to External");
      /*Look up mapping*/
      lookUp = sr_nat_lookup_internal(sr->nat, header_ip->ip_src, tcp_header->s_port, nat_mapping_tcp);
      /*If mapping does not exist, insert a mapping*/
      if (lookUp == NULL) {
	printf("Mapping does not exist");
	lookUp = sr_nat_insert_mapping(sr->nat, header_ip->ip_src, tcp_header->s_port, nat_mapping_tcp);
      }
      header_ip->ip_src = externalIP;
      header_ip->ip_ttl = header_ip->ip_ttl - 1;
      tcp_header->s_port = ntohs(lookUp->aux_ext);
      header_ip->ip_sum = 0;
      header_ip->ip_sum = cksum(header_ip, 20);
      /*tcp checksum stuff*/
      tcp_header->sum = 0 ;
      tcp_header->sum = cksum(tcp_header, 20);

      struct sr_if* sender;
      /*Set the new Source Mac Address*/
      sender = sr_get_interface(sr, testRT->interface);
      memcpy(ethernet_hdr->ether_shost, sender->addr, 6);

      /*Arp request for the next Destination*/
      struct sr_arpentry* entry;
      struct sr_rt* forwardRT;
      forwardRT = getPath(sr, header_ip->ip_dst);
      if (forwardRT){
		  
	entry = sr_arpcache_lookup(&(sr->cache), forwardRT->dest.s_addr);
      }
      else{
	entry = NULL;
      }
      if (entry){
		  
	memcpy(ethernet_hdr->ether_dhost, entry->mac, 6);
		  
	sr_send_packet(sr, packet, len, testRT->interface);
      }
      else{
	struct sr_arpreq* req;
	req = sr_arpcache_queuereq(&(sr->cache), header_ip->ip_dst,packet, len, testRT->interface);
	handle_arpreq(sr, req);
      }
    }
    else{
      printf("TCP from Extrernal to Internal\n");
      /*Look up mapping*/
      lookUp = sr_nat_lookup_external(sr->nat, ntohs(tcp_header->d_port), nat_mapping_icmp);
      /*If mapping does not exist, insert a mapping*/
      if (lookUp == NULL) {
	printf("ExMapping does not exist\n");
	sleep(6.0);
	struct sr_rt* forwardRT;
	forwardRT = getPath(sr, header_ip->ip_src);
	sendICMP(sr, packet, sr_get_interface(sr, forwardRT->interface),  forwardRT, header_ip->ip_src);
      }
      else{
	testRT = getPath(sr, header_ip->ip_dst);

	/*Rewrite TCP/IP header to prepare to send*/;

	tcp_header->d_port = lookUp->aux_int;
	header_ip->ip_ttl = header_ip->ip_ttl - 1;
	header_ip->ip_dst = lookUp->ip_int;

	header_ip->ip_sum = 0;
	header_ip->ip_sum = cksum(header_ip, 20);
	tcp_header->sum = 0 ;
	tcp_header->sum = cksum(tcp_header, 20);
	/*tcp checksum*/
	
	struct sr_if* sender;
	/*Set the new Source Mac Address*/
	sender = sr_get_interface(sr, "eth1");
	memcpy(ethernet_hdr->ether_shost, sender->addr, 6);
	struct sr_rt* forwardRT;
	forwardRT = getPath(sr, lookUp->ip_int);
	/*Arp request for the next Destination*/
	if (ntohs(tcp_header->d_port) < 1024){
	  sendICMP(sr, packet, sender, forwardRT, lookUp->ip_int);
	} 
	else{
	  struct sr_arpentry* entry;
      
	  if (forwardRT){
		  
	    entry = sr_arpcache_lookup(&(sr->cache), lookUp->ip_int);
	  }
	  else{
	    entry = NULL;
	  }
	  if (entry){
	    memcpy(ethernet_hdr->ether_dhost, entry->mac, 6);
	   
	    sr_send_packet(sr, packet, len, sender->name);
	  }
	  else{
	    struct sr_arpreq* req;
	    req = sr_arpcache_queuereq(&(sr->cache), lookUp->ip_int ,packet, len, "eth1");
	    handle_arpreq(sr, req);
	  }
	}
      }

      printf("Preparing to send packet to destination");
      /*print_nat_table(sr->nat);*/
      /*free(lookUp);*/
      /*Check if External*/
    }
  }
  

} 

void natSort(struct sr_instance* sr, uint8_t* packet, struct sr_if* inter, unsigned int len) {
  struct sr_ip_hdr* header_ip = (struct sr_ip_hdr*)(packet+sizeof(struct sr_ethernet_hdr));
	printf("1.%d 2.%d\n", header_ip->ip_p, ip_protocol_icmp);
	if (header_ip->ip_p == ip_protocol_icmp){
		/*DO ICMP THINGS*/
		printf("This is ICMP\n");
		natICMP(sr, packet, inter, len);
	}
	else if (header_ip->ip_p == 6){
		/*DO TCP THINGS*/
		printf("This is TCP");
		natTCP(sr, packet, inter, len);
	}
	else {
		printf("Received unknown packet");
	}
}

void transfer(uint8_t* source, uint8_t* change){
  int i;
  for (i=0; i<6; i++){
    source[i] = change[i];
  }
}

void transferChar(unsigned char* source, unsigned char* change){
  int i;
  for (i=0; i<6; i++){
    source[i] = change[i];
  }
}

uint8_t* makeICMP(struct sr_instance* sr, int type, int code){
  if (type == 4){
    uint8_t* packet = malloc(sizeof(struct sr_ip_hdr) + sizeof(struct sr_ethernet_hdr) + sizeof(struct sr_icmp_hdr));
    struct sr_ethernet_hdr* newEther = (struct sr_ethernet_hdr*) packet;
    struct sr_icmp_t3_hdr* newICMP = (struct sr_icmp_t3_hdr*) (packet + 34);
    newEther->ether_type = ntohs(0x0800);
    newICMP->icmp_type = type;
    newICMP->icmp_code = code;
    newICMP->icmp_sum = 0;
    newICMP->icmp_sum = cksum(newICMP, sizeof(struct sr_icmp_t3_hdr));

    return packet;
  } 
  else{
    uint8_t* packet = malloc(sizeof(struct sr_ip_hdr) + sizeof(struct sr_ethernet_hdr) + sizeof(struct sr_icmp_t3_hdr));
    struct sr_ethernet_hdr* newEther = (struct sr_ethernet_hdr*) packet;
    struct sr_ip_hdr* newIP = (struct sr_ip_hdr*) (packet+14);
    struct sr_icmp_t3_hdr* newICMP = (struct sr_icmp_t3_hdr*) (packet + 34);
    newEther->ether_type = ntohs(0x0800);
    newICMP->icmp_type = type;
    newICMP->icmp_code = code;
    newICMP->unused = 0;
    newICMP->next_mtu = 0;
    memcpy(newICMP->data, newIP, 28);
    newICMP->icmp_sum = 0;
    newICMP->icmp_sum = cksum(newICMP, sizeof(struct sr_icmp_t3_hdr));
    return packet;
  }

} 



/*---------------------------------------------------------------------
 * Method: sr_handlepacket(uint8_t* p,char* interface)
 * Scope:  Global
 *
 * This method is called each time the router receives a packet on the
 * interface.  The packet buffer, the packet length and the receiving
 * interface are passed in as parameters. The packet is complete with
 * ethernet headers.
 *
 * Note: Both the packet buffer and the character's memory are handled
 * by sr_vns_comm.c that means do NOT delete either.  Make a copy of the
 * packet instead if you intend to keep it around beyond the scope of
 * the method call.
 *
 *---------------------------------------------------------------------*/

void sr_handlepacket(struct sr_instance* sr,
		     uint8_t * packet/* lent */,
		     unsigned int len,
		     char* interface/* lent */)
{
  /* REQUIRES */
  assert(sr);
  assert(packet);
  assert(interface);

  if (sr->nat_flag == 1){
    if (sr->nat->ip_ext == -1){
      sr->nat->ip_ext = sr_get_interface(sr, "eth2")->ip;
    }
  }
  printf("*** -> Received packet of length %d \n",len);
  struct sr_ethernet_hdr* arp = (struct sr_ethernet_hdr*) packet;
  

  if (len < sizeof(sr_ethernet_hdr_t)) {
    printf("Length of Ethernet header too short, dropping\n");
    return;
  }
  if (ethertype(packet) == ethertype_ip) {
    struct sr_if* inter = sr_get_interface(sr, interface);
    if (sr->nat_flag == 1) {
      printf("NAT ENABLED");
      natSort(sr, packet, inter, len);
    }
    else{
      printf("This is an IP packet\n");
      struct sr_if* currentIF = sr->if_list; 
      struct sr_ip_hdr* ip = (struct sr_ip_hdr*)(packet+14);
      printf("huh?\n");
      print_addr_ip_int(ip->ip_dst);
      int ownIF = 0;
      int oldSum = ip->ip_sum;
      ip->ip_sum = 0;
      if (oldSum == cksum(ip,20)){

	while(currentIF){
	  if (ip->ip_dst == currentIF->ip){
	    ownIF=1;
	  }
	  currentIF = currentIF->next;
	}
	ip->ip_ttl = ip->ip_ttl-1;
	if (ip->ip_ttl<=-1){
	  struct sr_ip_hdr* encaps = (struct sr_ip_hdr*)(packet+14);
	  uint8_t* newBuffer = makeICMP(sr, 11, 0);
	  struct sr_ethernet_hdr* ether = (struct sr_ethernet_hdr*)newBuffer;
	  struct sr_ip_hdr* newIP = (struct sr_ip_hdr*)(newBuffer + 14);
	  struct sr_icmp_t3_hdr* newICMP = (struct sr_icmp_t3_hdr*)(newBuffer + 34);

	  memcpy(ether->ether_shost, arp->ether_dhost, 6);
	  memcpy(ether->ether_dhost, arp->ether_shost, 6);
	  struct sr_if* inter = sr_get_interface(sr, interface);
	  newIP->ip_src = inter->ip;
	  newIP->ip_dst = ip->ip_src;
	  newIP->ip_ttl = 64;
	  newIP->ip_hl = encaps->ip_hl;
	  newIP->ip_v = encaps->ip_v;
	  newIP->ip_tos = encaps->ip_tos;		        
	  newIP->ip_len = ntohs(0x0038);		        
	  newIP->ip_id = ntohs(0x0000);		        
	  newIP->ip_off = encaps->ip_off;
	  newIP->ip_p = 1;
	  newIP->ip_sum = 0;
	  newIP->ip_sum = cksum(newIP, 20);
	  
	  memcpy(newICMP->data, encaps, 28);
	  newICMP->icmp_sum = 0;
	  newICMP->icmp_sum = cksum(newICMP, sizeof(struct sr_icmp_t3_hdr));
	  sr_send_packet(sr, newBuffer,sizeof(struct sr_ip_hdr) + sizeof(struct sr_ethernet_hdr) + sizeof(struct sr_icmp_t3_hdr), interface);
	  free(newBuffer);

	}
	else{
	  if (ownIF==0){
	    struct sr_arpentry* entry;
	    print_hdrs(packet, 98);
	    struct sr_rt* testRT = getPath(sr, ip->ip_dst);
	    if (testRT){
	      entry = sr_arpcache_lookup(&(sr->cache), testRT->gw.s_addr);
	    }
	    else{
	      entry = NULL;
	    }
	    currentIF = sr->if_list;
       
	    if(entry == NULL){
	      if (testRT){
	      
		struct sr_if* currentIF = sr_get_interface(sr, testRT->interface);
		struct sr_ethernet_hdr* ether = (struct sr_ethernet_hdr*)packet;
		printf("THIS ONLY HAPPENS ONCE!\n\n\n");
		print_addr_eth(currentIF->addr);
		memcpy(ether->ether_shost, currentIF->addr, 6);
		ip->ip_sum = 0;
		ip->ip_sum = cksum(ip, 20);
		print_addr_ip_int(testRT->gw.s_addr);
		struct sr_arpreq* req = sr_arpcache_queuereq(&(sr->cache), testRT->gw.s_addr,packet, len, currentIF->name);
		handle_arpreq(sr, req);
	      }
	      else{
		struct sr_arpreq* req = sr_arpcache_queuereq(&(sr->cache), ip->ip_dst,packet, len, interface);
		handle_arpreq(sr, req);
	      }

	    }
	    else{
	      /*already has entry*/
	      testRT = getPath(sr, ip->ip_dst);
	      struct sr_ethernet_hdr* ether = (struct sr_ethernet_hdr*)packet;
	      struct sr_if* curIF = sr_get_interface(sr, testRT->interface);
	      printf("Look here\n\n");
	      print_addr_eth(curIF->addr);
	      memcpy(ether->ether_shost, curIF->addr, 6);
	      memcpy(ether->ether_dhost, entry->mac, 6);
	      ip->ip_sum = 0;
	      ip->ip_sum = cksum(ip, 20);
	      sr_send_packet(sr, packet, len , curIF->name);
	    }
	  }
	  else{
	    /*sent to own interface*/
	    if (ip->ip_p == 1){
	      /*work here*/
	      uint8_t* newBuffer = malloc(98);
	      struct sr_ip_hdr* encaps = (struct sr_ip_hdr*)(newBuffer+14);
	      memcpy(newBuffer+14, packet + 14, 20);
	      struct sr_ethernet_hdr* ether = (struct sr_ethernet_hdr*)newBuffer;
	      memcpy(newBuffer, packet, 14);
	      struct sr_icmp_t3_hdr* newICMP = (struct sr_icmp_t3_hdr*)(newBuffer + 34);
	      memcpy(newBuffer + 34, packet + 34, 64);
	      uint32_t placeholder;
	      newICMP->icmp_type = 0;
	      newICMP->icmp_code = 0;
	      
	  
	      encaps->ip_ttl = 255;
	  
	      memcpy(ether->ether_shost, ether->ether_dhost, 6);
	      placeholder = encaps->ip_dst;
	      encaps->ip_dst = encaps->ip_src;
	      encaps->ip_src = placeholder;
	      encaps->ip_sum = 0;
	      encaps->ip_sum = cksum(encaps, 20);
	      newICMP->icmp_sum = 0;
	      newICMP->icmp_sum = cksum(newICMP, 64);
	      /*HEY*/
	      struct sr_arpentry* entry;
	      struct sr_rt* forwardRT = getPath(sr, encaps->ip_dst);
	      if (forwardRT){
		  
		entry = sr_arpcache_lookup(&(sr->cache), forwardRT->gw.s_addr);
	      }
	      else{
		entry = NULL;
	      }
	      if (entry){
		  
		memcpy(ether->ether_dhost, entry->mac, 6);
		  
		sr_send_packet(sr, newBuffer, sizeof(struct sr_ip_hdr) + sizeof(struct sr_ethernet_hdr) + sizeof(struct sr_icmp_t3_hdr) + 28, forwardRT->interface);
	      }
	      else{
		struct sr_arpreq* req = sr_arpcache_queuereq(&(sr->cache), forwardRT->gw.s_addr ,newBuffer, len, forwardRT->interface);
		handle_arpreq(sr, req);
	      }
	  
	    } 
	    else{
	      /*drop packet*/
	      printf("test\n\n");
	      struct sr_ip_hdr* encaps = (struct sr_ip_hdr*)(packet+14);
	      uint8_t* newBuffer = makeICMP(sr, 3, 3);
	      struct sr_ethernet_hdr* ether = (struct sr_ethernet_hdr*)newBuffer;
	      struct sr_ip_hdr* newIP = (struct sr_ip_hdr*)(newBuffer + 14);
	      struct sr_icmp_t3_hdr* newICMP = (struct sr_icmp_t3_hdr*)(newBuffer + 34);

	      memcpy(ether->ether_shost, arp->ether_dhost, 6);
	      memcpy(ether->ether_dhost, arp->ether_shost, 6);
	      
	      
	      newIP->ip_dst = ip->ip_src;
	      newIP->ip_src = ip->ip_dst;
	      print_addr_ip_int(ip->ip_src);
	      newIP->ip_ttl = 255;
	      newIP->ip_hl = encaps->ip_hl;
	      newIP->ip_v = encaps->ip_v;
	      newIP->ip_tos = encaps->ip_tos;		        
	      newIP->ip_len = ntohs(0x0038);		        
	      newIP->ip_id = ntohs(0x0000);		        
	      newIP->ip_off = encaps->ip_off;
	      newIP->ip_p = 1;
	      
	      newIP->ip_sum = 0;
	      newIP->ip_sum = cksum(newIP, 20);
	  
	      memcpy(newICMP->data, encaps, 28);
	      newICMP->icmp_sum = 0;
	      newICMP->icmp_sum = cksum(newICMP, sizeof(struct sr_icmp_t3_hdr));
	      struct sr_rt* newInterface = getPath(sr, newIP->ip_dst);
	      sr_send_packet(sr, newBuffer,70, newInterface->interface);
	      free(newBuffer);
	    }

	  }
	}
	
      }
    }
  }
  else if (ethertype(packet) == ethertype_arp) {
    printf("This is an ARP packet \n");

    struct sr_arp_hdr*  encaps = (struct sr_arp_hdr*)(packet + 14); 
    uint8_t* newBuffer = malloc(sizeof(struct sr_ethernet_hdr) + sizeof(struct sr_arp_hdr));
    struct sr_ethernet_hdr* newEther = (struct sr_ethernet_hdr*)newBuffer;
    struct sr_arp_hdr* newArp = (struct sr_arp_hdr*)(newBuffer + 14);

    struct sr_if* currentIF = sr_get_interface(sr, interface);

    transfer(newEther->ether_dhost, arp->ether_shost);
    transfer(newEther->ether_shost, currentIF->addr);

    /*If a arp request*/
    if (ntohs(encaps->ar_op) == 1) {
      newArp->ar_hrd = encaps->ar_hrd;
      newArp->ar_pro = encaps->ar_pro;
      newArp->ar_hln = encaps->ar_hln;
      newArp->ar_pln = encaps->ar_pln;
      newArp->ar_op = 512;
      newArp->ar_sip = encaps->ar_tip;
      newArp->ar_tip = encaps->ar_sip;
      transferChar(newArp->ar_sha, currentIF->addr);
      transferChar(newArp->ar_tha, encaps->ar_sha);
      newEther->ether_type = arp->ether_type;
      sr_send_packet(sr,newBuffer, sizeof(struct sr_ethernet_hdr) + sizeof(struct sr_arp_hdr), interface);

    }
    /*If arp reply*/
    else{
      struct sr_arpentry* ent = sr_arpcache_lookup(&(sr->cache), encaps->ar_sip);
      if (ent == NULL){

	struct sr_arpreq* req = sr_arpcache_insert(&(sr->cache), encaps->ar_sha, encaps->ar_sip);
	if (req!=NULL){
	  empty_req(sr, req);
	  sr_arpreq_destroy(&(sr->cache),req);
	}
      }
      else{
	free(ent);
      }
    }
      
      
  }
  else {
    printf("This is an unrecognized packet\n");
  }
}

/* end sr_ForwardPacket */
